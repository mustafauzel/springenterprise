package be.mustafa.spring.orders;

import be.mustafa.spring.orders.BeerOrder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BeerOrderRepository extends JpaRepository<BeerOrder, Integer> {
    default int saveOrder(BeerOrder order){
        return save(order).getId();
    }
    default BeerOrder getBeerOrderById(int id){
        return findById(id).orElse(null);
    }
}
