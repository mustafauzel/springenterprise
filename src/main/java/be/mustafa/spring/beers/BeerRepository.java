package be.mustafa.spring.beers;

import be.mustafa.spring.beers.Beer;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.RepositoryDefinition;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


public interface BeerRepository extends JpaRepository<Beer, Integer> {  //Doelobject en Type van de primary key
    default Beer getBeerById(int id){
        return findById(id).orElse(null);
    }

    public default void updateBeer(Beer b){
        save(b);
    }

    @Query("select b from Beer b where b.alcohol = ?1")
    List<Beer> getBeerByAlcohol(double alcohol);

    List<Beer> getBeersByNameContainingOrderByNameAsc(String name);

    List<Beer> getBeersByStockLessThanEqual(int stock);

    @Query("update Beer b set b.price = b.price + ( b.price * ((:increasePercent / 100.00)))")
    @Modifying
    void updatePriceWithPercent(@Param("increasePercent") double increasePercent);

    default List<Beer> getBeersWithAlcoholAndName(double alcohol, String nameStartsWith){
        ExampleMatcher matcher = ExampleMatcher.matchingAll().withIgnorePaths("version", "price", "brewer", "category", "id", "stock")
                .withMatcher("name", m -> m.ignoreCase().startsWith());

        Beer probe = new Beer();
        probe.setAlcohol(alcohol);
        probe.setName(nameStartsWith);

        Example<Beer> example = Example.of(probe, matcher);

        List<Beer> list = findAll(example);
        return list;

    }
}
