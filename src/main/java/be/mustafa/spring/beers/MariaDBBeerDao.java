package be.mustafa.spring.beers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository("mariaDBImpl")
public class MariaDBBeerDao implements BeerDao{
    private final static String QUERY_ID = "select Name,Alcohol,Price,Stock from Beers where Id = ?";
    private final static String UPDATE_STOCK = "update Beers set stock = ? where Id = ?";
    private final static String QUERY_ALCOHOL = "select Name, Alcohol, Price, Stock from Beers where Alcohol = ?";

    JdbcTemplate template;
    @Override
    public String getBeerById(int id) {
        String beer = "";
        Map<String, Object> result = template.queryForMap(QUERY_ID, id);
        beer = String.format("%s %s %s %s",
                result.get("name"),
                result.get("alcohol"),
                result.get("price"),
                result.get("stock"));
        return beer;
    }

    @Override
    public void setStock(int id, int stock) {
        template.update(UPDATE_STOCK, stock, id);
    }

    @Autowired
    public void setTemplate(JdbcTemplate template){
        this.template = template;
    }

    @Override
    public List<String> getBeersByAlcohol(double alcohol) {
        List<Map<String, Object>> resultlist = template.queryForList(QUERY_ALCOHOL, alcohol);
        return resultlist.stream().map(this::mapToBeerString).collect(Collectors.toList());
    }

    private String mapToBeerString(Map<String, Object> beerMap){
        return String.format("%s %s %s %s",
                beerMap.get("name"),
                beerMap.get("alcohol"),
                beerMap.get("price"),
                beerMap.get("stock"));
    }
}
