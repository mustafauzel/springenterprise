package be.mustafa.spring.brewers;

import be.mustafa.spring.beers.Beer;
import org.w3c.dom.ls.LSOutput;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "Brewers")
public class Brewer implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private int id;
    @Column(name = "Name")
    private String name;
    @Column(name = "Address")
    private String address;
    @Column(name = "ZipCode")
    private String zipCode;
    @Column(name = "City")
    private String city;
    @Column(name = "Turnover")
    private int turnover;
    @OneToMany(mappedBy = "brewer")
    private Set<Beer> beers = new HashSet<>();

    public Brewer() {
    }

    public int getId() {
        return id;
    }

    void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getTurnover() {
        return turnover;
    }

    public void setTurnover(int turnover) {
        this.turnover = turnover;
    }

    public Set<Beer> getBeers() {
        return beers;
    }

    public void setBeers(Set<Beer> beers) {
        this.beers = beers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Brewer brewer = (Brewer) o;
        return id == brewer.id &&
                turnover == brewer.turnover &&
                Objects.equals(name, brewer.name) &&
                Objects.equals(address, brewer.address) &&
                Objects.equals(zipCode, brewer.zipCode) &&
                Objects.equals(city, brewer.city) &&
                Objects.equals(beers, brewer.beers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, address, zipCode, city, turnover, beers);
    }

    @Override
    public String toString() {
        return "Brewer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", city='" + city + '\'' +
                ", turnover=" + turnover +
                ", beers=" + beers +
                '}';
    }
}
