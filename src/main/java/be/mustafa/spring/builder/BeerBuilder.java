package be.mustafa.spring.builder;


import be.mustafa.spring.beers.Beer;

public class BeerBuilder {
    private Beer beer;

    public BeerBuilder(){
        this.beer = new Beer();
    }

    public BeerBuilder withName(String name){
        return this.setName(name);
    }
    public BeerBuilder withPrice(double price){
        return this.setPrice(price);
    }

    public BeerBuilder withStock(int stock){
        return this.setStock(stock);
    }
    public BeerBuilder withAlcohol(double alcohol){
        return this.setAlcohol(alcohol);
    }

    private BeerBuilder setName(String name){
        beer.setName(name);
        return this;
    }
    private BeerBuilder setPrice(double price){
        beer.setPrice(price);
        return this;
    }
    private BeerBuilder setStock(int stock){
        beer.setStock(stock);
        return this;
    }
    private BeerBuilder setAlcohol(double alcohol){
        beer.setAlcohol(alcohol);
        return this;
    }

    public Beer build(){
        return beer;
    }
}
