package be.mustafa.spring.services;

import be.mustafa.spring.beers.Beer;
import be.mustafa.spring.beers.BeerRepository;
import be.mustafa.spring.exceptions.BeerOutOfStockException;
import be.mustafa.spring.exceptions.InvalidBeerException;
import be.mustafa.spring.exceptions.InvalidNumberException;
import be.mustafa.spring.orders.BeerOrder;
import be.mustafa.spring.orders.BeerOrderItem;
import be.mustafa.spring.orders.BeerOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.amqp.AbstractRabbitListenerContainerFactoryConfigurer;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class BeerServiceImpl implements BeerService {
    private BeerOrderRepository orderRepo;
    private BeerRepository beerRepo;

    @Autowired
    public void setBeerOrderRepository(BeerOrderRepository orderRepo){
        this.orderRepo = orderRepo;
    }
    @Autowired
    public void setBeerRepository(BeerRepository beerRepo){
        this.beerRepo = beerRepo;
    }

    @Override
    @Transactional(rollbackFor = {BeerOutOfStockException.class, InvalidBeerException.class, InvalidNumberException.class})
    public int orderBeer(String name, int beerId, int number) {
        /*Beer beer = getBeer(beerId);
        if(number < 1){
            throw new InvalidNumberException("Order amount: You must atleast order 1 beer.");
        }
        if(beer.getStock() < number){
            throw new BeerOutOfStockException("Beer Stock: Nog enough beer in stock");
        }

        beer.setStock(beer.getStock() - number);
        BeerOrder order = new BeerOrder();
        order.setName(name);
        List<BeerOrderItem> orderItems = new ArrayList<>();
        BeerOrderItem item = new BeerOrderItem();
        item.setBeer(beer);
        item.setNumber(number);
        orderItems.add(item);
        order.setItems(orderItems);

        beerRepo.updateBeer(beer);
        orderRepo.saveOrder(order);

        return order.getId();*/
        return this.orderBeers(name, new int[][] {{beerId, number}});
    }

    @Override
    @Transactional(rollbackFor = {BeerOutOfStockException.class, InvalidBeerException.class, InvalidNumberException.class})
    public int orderBeers(String name, int[][] ordered) {
        BeerOrder order = new BeerOrder();
        order.setName(name);

        List<BeerOrderItem> orderItems = new ArrayList<>();

        for(int[] el: ordered){
            int beerId = el[0];
            int number = el[1];
            Beer beer = getBeer(beerId);
            if (number < 0) {
                throw new InvalidNumberException("Order amount: Number is less than 0");
            }
            if (beer.getStock() < number) {
                throw new BeerOutOfStockException("Beer Stock: Not enough beer in stock");
            }

            beer.setStock(beer.getStock() - number);

            BeerOrderItem item = new BeerOrderItem();
            item.setBeer(beer);
            item.setNumber(number);
            orderItems.add(item);

            beerRepo.updateBeer(beer);
        }

        order.setItems(orderItems);
        orderRepo.saveOrder(order);
        return order.getId();
    }

    private Beer getBeer(int id) throws InvalidBeerException{
        return Optional.ofNullable(beerRepo.getBeerById(id)).orElseThrow(() -> new InvalidBeerException("Beer not found"));
    }
}
