package be.mustafa.spring.beers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@DirtiesContext
class BeerDaoTest {
    private static final String TEST_BEER = "TestBeer 7.0 2.75 100";
    @Autowired
    BeerDao dao;

    @Test
    void testGetBeerById(){
        String beer = dao.getBeerById(1);
        assertEquals(TEST_BEER, beer);
    }

    @Test
    void getBeerByIdReturnsString(){
        assertEquals(dao.getBeerById(1).getClass(), String.class);
    }

    @Test
    void getBeerByIdReturnsNotNull(){
        assertNotNull(dao.getBeerById(1));
    }

    @Test
    void setStock(){
        int stock = 300;
        dao.setStock(1,stock);
        String beer = dao.getBeerById(1);
        String[] list = beer.split(" ");
        assertEquals(list[list.length -1], Integer.toString(stock));
    }

    @Test
    void getBeersByAlcohol(){
        assertTrue(ArrayList.class.isInstance(dao.getBeersByAlcohol(7)));
    }


}
