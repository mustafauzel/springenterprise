package be.mustafa.spring.services;

import be.mustafa.spring.beers.Beer;
import be.mustafa.spring.beers.BeerRepository;
import be.mustafa.spring.exceptions.BeerOutOfStockException;
import be.mustafa.spring.exceptions.InvalidBeerException;
import be.mustafa.spring.exceptions.InvalidNumberException;
import be.mustafa.spring.orders.BeerOrder;
import be.mustafa.spring.orders.BeerOrderItem;
import be.mustafa.spring.orders.BeerOrderRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
@DirtiesContext
class BeerServiceTest {
    @Autowired
    private BeerService service;
    @Autowired
    private BeerRepository beerRepo;
    @Autowired
    private BeerOrderRepository orderRepo;

    @Test
    void orderBeer(){
        String name = "BeerNameTest";
        int beerId = 1;
        int number = 3;

        int orderId = service.orderBeer(name, beerId, number);

        assertNotEquals(0, orderId);
        assertEquals(orderId, orderRepo.getBeerOrderById(orderId).getId());
        assertEquals(name, orderRepo.getBeerOrderById(orderId).getName());
        BeerOrder beerOrder = orderRepo.getBeerOrderById(orderId);
        assertEquals(name, beerOrder.getName());

        BeerOrderItem item = beerOrder.getItems().get(0);
        assertEquals(number, item.getNumber());

        Beer beer = item.getBeer();
        assertEquals(beerId, beer.getId());
    }

    @Test
    void orderWithExceededNumberThrowsException(){
        String name = "BeerNameTest";
        int beerId = 1;
        int number = 300;
        assertThrows(BeerOutOfStockException.class, () -> service.orderBeer(name, beerId, number));
    }

    @Test
    void orderWithNumberBelowZero(){
        String name = "BeerNameTest";
        int beerId = 1;
        int number = -3;
        assertThrows(InvalidNumberException.class, () -> service.orderBeer(name, beerId, number));
    }

    @Test
    void orderBeerThatDoesntExist(){
        String name = "BeerNameTest";
        int beerId = 6;
        int number = 3;
        assertThrows(InvalidBeerException.class, () -> service.orderBeer(name, beerId, number));
    }

    @Test
    void orderMultipleBeers(){
        String name = "BeerNameTest";
        int[][] orders = {{1,3}, {1,3}};

        service.orderBeers(name, orders);
        assertEquals(94, beerRepo.getBeerById(1).getStock());
    }

    @Test
    void orderBeerRollsBackAfterInvalidAmountException(){
        int idAfterInvalidAmount = 0;

        try{
            idAfterInvalidAmount = service.orderBeer("MyTestOrder", 1, 200);
        }catch (Exception e){
        }
        assertEquals(0, idAfterInvalidAmount);
        assertEquals(100, beerRepo.getBeerById(1).getStock());
    }

    @Test
    void orderBeerRollsBackAfterInvalidBeerException(){
        int idAfterInvalidBeer = 0;

        try{
            idAfterInvalidBeer = service.orderBeer("MyTestOrder", 6, 50);
        }catch (Exception e){}
        assertEquals(0,idAfterInvalidBeer);
        assertEquals(100, beerRepo.getBeerById(1).getStock());
    }

}
