package be.mustafa.spring.services;

import be.mustafa.spring.beers.BeerRepository;
import be.mustafa.spring.orders.BeerOrder;
import be.mustafa.spring.orders.BeerOrderItem;
import be.mustafa.spring.orders.BeerOrderRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class BeerOrderRepositoryTest {
    @Autowired
    BeerOrderRepository repo;
    @Autowired
    BeerRepository beerRepository;

    @Test
    void saveOrder(){
        BeerOrder order = new BeerOrder();
        order.setName("TestOrder2");
        List<BeerOrderItem> itemList = new ArrayList<>();
        BeerOrderItem item = new BeerOrderItem();
        item.setNumber(2);
        item.setBeer(beerRepository.getBeerById(1));
        itemList.add(item);
        order.setItems(itemList);

        int id = repo.saveOrder(order);
        assertEquals(order, repo.getBeerOrderById(id));
    }

    @Test
    void getBeerOrderById(){
        BeerOrder order = repo.getBeerOrderById(1);
        assertEquals("TestOrder", order.getName());
    }





}
