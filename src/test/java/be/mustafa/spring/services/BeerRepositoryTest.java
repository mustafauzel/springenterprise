package be.mustafa.spring.services;

import be.mustafa.spring.beers.Beer;
import be.mustafa.spring.beers.BeerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class BeerRepositoryTest {
    private static final int BREWER_ID = 1;
    private static final int CATEGORY_ID = 1;

    private Beer beer;

    @BeforeEach
    void init(){
        beer = new Beer();
        beer.setName("TestBeer");
        beer.setPrice(2.75);
        beer.setStock(100);
        beer.setAlcohol(7);
    }

    @Autowired
    private BeerRepository repo;

    @Test
    void getBeerById(){
        Beer beerTest = repo.getBeerById(1);
        assertEquals(beer.getName(), beerTest.getName());
        assertEquals(beer.getPrice(), beerTest.getPrice());
        assertEquals(BREWER_ID, beerTest.getBrewer().getId());
        assertEquals(CATEGORY_ID, beerTest.getCategory().getId());
    }

    @Test
    void getBeerByIdReturnsClassBeer(){
        Beer beerTest = repo.getBeerById(1);
        assertEquals(Beer.class, beerTest.getClass());
    }

    @Test
    void getBeerByIdReturnsNotNull(){
        Beer testbeer = repo.getBeerById(1);
        assertNotNull(testbeer);
    }

    @Test
    void getBeerByAlcohol(){
        List<Beer> beerlist = new ArrayList<>();
        beerlist.add(beer);
        beerlist.add(beer);
        beerlist.add(beer);

        List<Beer> beerListDataBank = repo.getBeerByAlcohol(7);
        assertEquals(beerlist.size(), beerListDataBank.size());
        assertEquals(beerlist.get(0).getName(), beerListDataBank.get(0).getName());
    }

    @Test
    void updateBeer(){
        int stock = 300;
        Beer beerUpdate = repo.getBeerById(1);
        beerUpdate.setStock(stock);
        beerUpdate.setPrice(5.0);

        repo.updateBeer(beerUpdate);
        assertEquals(stock, repo.getBeerById(1).getStock());
        assertEquals(5.0, repo.getBeerById(1).getPrice());
        assertEquals(beer.getName(), repo.getBeerById(1).getName());
    }

    @Test
    void getBeerWhereNameLike(){
        List<Beer> list = repo.getBeersByNameContainingOrderByNameAsc("Test");
        assertEquals("TestA", list.get(0).getName());
        assertEquals("TestBeer", list.get(1).getName());
        assertEquals("TestBeer2", list.get(2).getName());
    }

    @Test
    void getBeersWithLessStockThan(){
        int stock = 50;
        List<Beer> list = repo.getBeersByStockLessThanEqual(stock);
        list.stream().forEach(el -> assertTrue(stock >= el.getStock()));
        assertEquals(1, list.size());
    }

    @Test
    void updatePriceWithPercent(){
        double percentage = 100;
        repo.updatePriceWithPercent(percentage);
        List<Beer> list = repo.findAll();
        list.stream().forEach(b -> assertEquals(5.50, b.getPrice()));
    }

    @Test
    void getBeersWithNameAndAlcohol(){
        double alcohol = 7;
        String name = "Test";
        List<Beer> list = repo.getBeersWithAlcoholAndName(alcohol, name);
        assertEquals(3, list.size());
    }
}
